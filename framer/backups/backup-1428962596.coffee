{VideoAdUnit} = require 'videoAdUnit'

isScrolling = false

static_news_feed = new Layer({
	width: Screen.width,
	height: Screen.height
	image: 'images/news_feed.png'
})

ad_hit_target = new Layer({
	name: 'Ad Hit Target',
	x: 36,
	y: 210,
	width: 568,
	height: 656,
	backgroundColor: 'limegreen',
	opacity: 0.0
	scale: 0.8
	originX: 0.5
	originY: 0.5
})

ad_hit_target.on Events.Click, (event, layer) ->
	videos_container.visible = true
	videos_container.animate({
		properties: {
			scale: 1.0,
			opacity: 1.0
		},
		timing: 0.05
	})		
	videos_container.content.subLayers[0].video.player.play()

videos_container = new PageComponent({
	backgroundColor: 'black',
	opacity: 0.4,
	width: Screen.width,
	height: Screen.height,
	visible: false,
	scale: 0.8,
	scrollVertical: false,
})

video_playlist = [
	'01-game-of-war.mp4',
	'1536x2048_Rep30Port.mp4',
	'vid1.mov',
	'vid3.mov'
]

for video in video_playlist
	ad = new VideoAdUnit({
		video: "videos/#{video}",
		height: Screen.height,
		width: Screen.width,
		backgroundColor: 'black'
	})
	videos_container.addPage(ad, 'right')

previous_video = videos_container.content.subLayers[0].video

videos_container.on Events.Scroll, ->
	isScrolling = true

videos_container.on Events.Click, (event, layer) ->
	video = @currentPage.video
	if isScrolling == false
		if video.player.paused == true 
			video.player.play() 
		else 
			video.player.pause()
	else
		isScrolling = false

videos_container.on "change:currentPage", ->
	if previous_video != @currentPage.video
		isScrolling = false
		previous_video.player.pause()
		previous_video = @currentPage.video
		@currentPage.video.player.currentTime = 0.0
		@currentPage.video.player.play()