require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"videoAdUnit":[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

exports.VideoAdUnit = (function(superClass) {
  extend(VideoAdUnit, superClass);

  function VideoAdUnit(options) {
    if (options == null) {
      options = {};
    }
    VideoAdUnit.__super__.constructor.call(this, options);
    this.video = new VideoLayer({
      video: options.video,
      width: options.width,
      height: options.height,
      backgroundColor: options.backgroundColor
    });
    this.video.player.loop = true;
    this.addSubLayer(this.video);
    print(this.subLayers);
  }

  return VideoAdUnit;

})(Layer);



},{}]},{},[])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvVXNlcnMvYWtpcmt3b29kL1dvcmtiZW5jaC9Db2RlL1BvcnRyYWl0IFZpZGVvL25hdGl2ZS1wb3J0cmFpdC12aWRlby1hZHMuZnJhbWVyL21vZHVsZXMvdmlkZW9BZFVuaXQuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUEsSUFBQTs2QkFBQTs7QUFBQSxPQUFhLENBQUM7QUFDYixpQ0FBQSxDQUFBOztBQUFhLEVBQUEscUJBQUMsT0FBRCxHQUFBOztNQUFDLFVBQVE7S0FDbkI7QUFBQSxJQUFBLDZDQUFNLE9BQU4sQ0FBQSxDQUFBO0FBQUEsSUFFQSxJQUFDLENBQUEsS0FBRCxHQUFhLElBQUEsVUFBQSxDQUFXO0FBQUEsTUFDcEIsS0FBQSxFQUFPLE9BQU8sQ0FBQyxLQURLO0FBQUEsTUFFcEIsS0FBQSxFQUFPLE9BQU8sQ0FBQyxLQUZLO0FBQUEsTUFHcEIsTUFBQSxFQUFRLE9BQU8sQ0FBQyxNQUhJO0FBQUEsTUFJcEIsZUFBQSxFQUFpQixPQUFPLENBQUMsZUFKTDtLQUFYLENBRmIsQ0FBQTtBQUFBLElBUUEsSUFBQyxDQUFBLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBZCxHQUFxQixJQVJyQixDQUFBO0FBQUEsSUFVQSxJQUFDLENBQUEsV0FBRCxDQUFhLElBQUMsQ0FBQSxLQUFkLENBVkEsQ0FBQTtBQUFBLElBWUEsS0FBQSxDQUFNLElBQUMsQ0FBQSxTQUFQLENBWkEsQ0FEVTtFQUFBLENBQWI7O3FCQUFBOztHQURpQyxNQUFsQyxDQUFBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImNsYXNzIGV4cG9ydHMuVmlkZW9BZFVuaXQgZXh0ZW5kcyBMYXllclxuXHRjb25zdHJ1Y3RvcjogKG9wdGlvbnM9e30pIC0+XG4gICAgc3VwZXIgb3B0aW9uc1xuXG4gICAgQHZpZGVvID0gbmV3IFZpZGVvTGF5ZXIoe1xuICAgICAgICB2aWRlbzogb3B0aW9ucy52aWRlbyxcbiAgICAgICAgd2lkdGg6IG9wdGlvbnMud2lkdGgsXG4gICAgICAgIGhlaWdodDogb3B0aW9ucy5oZWlnaHQsXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogb3B0aW9ucy5iYWNrZ3JvdW5kQ29sb3JcbiAgICB9KVxuICAgIEB2aWRlby5wbGF5ZXIubG9vcCA9IHRydWVcblxuICAgIEBhZGRTdWJMYXllcihAdmlkZW8pXG5cbiAgICBwcmludCBAc3ViTGF5ZXJzXG4iXX0=
