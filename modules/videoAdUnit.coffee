class exports.VideoAdUnit extends Layer
	constructor: (options={}) ->
    super options

    @video = new VideoLayer({
        video: options.video,
        width: options.width,
        height: options.height,
        backgroundColor: options.backgroundColor
    })
    @video.player.loop = true

    @addSubLayer(@video)

    print @subLayers
